# Prometheus

Run metric exporter in python and prometheus locally.

```bash
# Install python requirements
pip install -r requirements.txt

# Run prometheus
docker-compose up

# Run exporter locally on port 8000
python run.py
```
