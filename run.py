from prometheus_client import start_http_server, Counter, Gauge
import time


g = Gauge('asdf', 'Description of counter')

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)

    c = 0
    g.set(c)

    while True:
        c += 30
        g.set(c)
        time.sleep(5)
